#include "i2c_flash.h"

static struct file_operations i2c_flash_fops = {
	.owner		= THIS_MODULE,
	.open		= i2c_flash_open,
	.release	= i2c_flash_release,
	.write 		= i2c_flash_write,
	.read		= i2c_flash_read,
	.unlocked_ioctl	= i2c_flash_ioctl,
};

long i2c_flash_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){
	printk("INSIDE I2C FLASH IOCTL\n");
	switch(cmd){
		case I2C_FLASHGETS: printk("GET I2C FLASH STATUS\n");
				put_user(0, (int*)arg);
				break;
		case I2C_FLASHGETP: printk("GET I2C FLASH POINTER\n");
				put_user(i2c_flash_devp->page_nr, (int*)arg);
				break;
		case I2C_FLASHSETP: printk("SET I2C FLASH PAGE POINTER\n");
				if(arg > 511){
					printk("INVALID PAGE NUMBER\n");
					return -1;
				}
				i2c_flash_devp->page_nr = (short int)arg * 64;
				printk("PAGE POINTER:%x", i2c_flash_devp->page_nr);
				i2c_flash_devp->msb = (i2c_flash_devp->page_nr) & 0xff;
				i2c_flash_devp->lsb = (i2c_flash_devp->page_nr >> 8) & 0xff;
				
				break;
		default: printk("INVALID I2C FLASH COMMAND\n");
			 return -EINVAL;
	}
	return 0;
}

ssize_t i2c_flash_write(struct file *file, const char *buf,
		size_t count, loff_t *ppos){
	int copied;
	int ret;
	struct i2c_msg i2c_msg;
	char *usr_buf;

	usr_buf = kmalloc(count, GFP_KERNEL);
	copied = copy_from_user(usr_buf, buf, count);
	if(copied == 0){
		printk("SUCCESSFULLY COPIED\n");
		printk("USER MESSAGE: %s\n", usr_buf);
	}
	else{
		printk("USR MSG COPY ERROR\n");
		return -ENOMEM;
	}

	i2c_msg.addr  = EEPROM_I2C_ADDR;
	i2c_msg.flags = 0;
	i2c_msg.len   = count;
	i2c_msg.buf   = usr_buf;
	ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_msg, 1);
	if(ret < 0){
		printk("I2C TRANSFER ERROR:%d\n", ret);
		return -1;
	}
	return 0;
}

ssize_t i2c_flash_read(struct file *file, char *buf,
		size_t count, loff_t *ppos){
	int ret;
	char *usr_buf;	
	int bytes;
	struct i2c_msg i2c_msg;
	usr_buf = kmalloc(count, GFP_KERNEL);

	i2c_msg.addr     = EEPROM_I2C_ADDR;
	i2c_msg.flags    = I2C_M_RD;
	i2c_msg.len      = count;
	i2c_msg.buf      = usr_buf;

	ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_msg, 2);
	if(ret < 0){
		printk("I2C TRANSFER ERROR\n");
		return -1;
	}
	bytes = copy_to_user(buf, usr_buf, count); 
	if(bytes == 0){
		printk("SUCCESS\n");
	}
	else{
		printk("COPY TO USER ERROR\n");
	}
	return 0;
}
		
static int i2c_flash_open(struct inode *inode, struct file *file){
	return 0;
}

static int i2c_flash_release(struct inode *inode, struct file *file){
	return 0;
}

int __init i2c_flash_init(void){
	int ret;
	int error, device_num;
	struct i2c_adapter *i2c_flash_adapter;
	struct i2c_client *i2c_flash_client;

	ret = gpio_request_one(I2C_MUX, GPIOF_OUT_INIT_LOW, "i2c_flash_pin");
	if(ret != 0){
		printk("GPIO EXPORT ERROR\n");
		return -1;
	}	

	gpio_set_value_cansleep(I2C_MUX, 0);

	if(alloc_chrdev_region(&i2c_flash_major_number, 0, 1, DEVICE_NAME) < 0){
		printk(KERN_DEBUG "Can't register device\n");
		return -1;
	}

	i2c_flash_class = class_create(THIS_MODULE, DEVICE_NAME);
	device_num = MKDEV(MAJOR(i2c_flash_major_number), i2c_flash_minor_number);
	i2c_flash_devp = kmalloc(sizeof(struct i2c_flash_dev), GFP_KERNEL);
	if(!i2c_flash_devp){
		printk("I2C FLASH DEVP ERROR\n");
		return -ENOMEM;
	}

	cdev_init(&i2c_flash_devp->cdev, &i2c_flash_fops);
	i2c_flash_devp->cdev.owner	= THIS_MODULE;
	i2c_flash_devp->devno	 	= device_num;
	error = cdev_add(&i2c_flash_devp->cdev, device_num, 1);
	if(error){
		printk("CDEV ADD ERROR\n");
		return -1;
	}

	i2c_flash_devp->dev = device_create(i2c_flash_class, NULL, device_num, NULL, "i2c_flash");

	i2c_flash_adapter = i2c_get_adapter(0);
	if(!i2c_flash_adapter){
		printk("I2C GET ADAPTER ERROR\n");
		return -1;
	}
	i2c_flash_devp->i2c_flash_adapter = i2c_flash_adapter;

	i2c_flash_client = i2c_new_device(i2c_flash_adapter, &i2c_flash_info);
	if(!i2c_flash_client){
		printk("I2C FLASH CLIENT NEW DEVICE ERROR\n");
		i2c_put_adapter(i2c_flash_adapter);
		return -1;
	}

	i2c_flash_devp->i2c_flash_client = i2c_flash_client;	
	i2c_flash_devp->page_nr = 0;
	printk("I2C FLASH DRIVER INITIALIZED\n");	
	return 0;
}

void __exit i2c_flash_exit(void){
	gpio_free(I2C_MUX);
	cdev_del(&i2c_flash_devp->cdev);

	i2c_unregister_device(i2c_flash_devp->i2c_flash_client);
	i2c_put_adapter(i2c_flash_devp->i2c_flash_adapter);
	device_destroy(i2c_flash_class, i2c_flash_devp->devno);
	unregister_chrdev_region((i2c_flash_major_number), 1);

	kfree(i2c_flash_devp);
	
	class_destroy(i2c_flash_class);
	printk("\nI2C FLASH DRIVER REMOVED");
}

module_init(i2c_flash_init);
module_exit(i2c_flash_exit);
MODULE_LICENSE("GPL v2");

#include "i2c_flash.h"

static struct file_operations i2c_flash_fops = {
	.owner		= THIS_MODULE,
	.open		= i2c_flash_open,
	.release	= i2c_flash_release,
	.write 		= i2c_flash_write,
	.read		= i2c_flash_read,
	.unlocked_ioctl = i2c_flash_ioctl,
};

long i2c_flash_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){
	int page_number, ret, i, addr_calc;
	char *erase_buf;
	char set_page_buf[2];
	struct i2c_msg i2c_erase_msg, i2c_setp_msg;
	switch(cmd){
		case FLASHGETS: put_user(0, (int*)arg);
				break;
		case FLASHGETP: page_number = (unsigned long)i2c_flash_devp->page_nr;
				ret = copy_to_user((void *)arg, &page_number, sizeof(int));
				if(ret < 0){
					return -1;
				}
				break;
		case FLASHSETP: page_number = (int)arg;
				if(page_number > 511){
					printk("INVALID PAGE NUMBER\n");
					return -1;
				}
				i2c_flash_devp->page_nr = page_number;
				set_page_buf[1] = (unsigned char)(page_number & 0xff);
				page_number &= 0xff00;
				set_page_buf[0] = (unsigned char)((page_number >> 8) & 0x7f);

				i2c_setp_msg.addr       = EEPROM_I2C_ADDR;
				i2c_setp_msg.flags    	= 0;
				i2c_setp_msg.len      	= 2;
				i2c_setp_msg.buf      	= set_page_buf;
			
				ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_setp_msg, 1);
				if(ret < 0){
					printk("ERROR IN SETTING PAGE POINTER\n");	
					return -1;
				}	
				break;
		case FLASHERASE:erase_buf = kmalloc(EEPROM_PAGE_SIZE, GFP_KERNEL);
				memset(erase_buf, '1', EEPROM_PAGE_SIZE);
				i2c_erase_msg.addr  = EEPROM_I2C_ADDR;
				i2c_erase_msg.len   = EEPROM_PAGE_SIZE + 2;
				i2c_erase_msg.flags = 0;
				page_number = 0;
				for(i = 0; i < EEPROM_SIZE; i++){
					addr_calc = page_number;
					addr_calc *= 64;
					erase_buf[1] = (unsigned char)(addr_calc & 0xff);
					addr_calc &= 0xff00;
					erase_buf[0] = (unsigned char)((addr_calc >> 8) & 0x7f);
					i2c_erase_msg.buf   = erase_buf;

					ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_erase_msg, 1);
					msleep(1);
					if(ret < 0){
						printk("ERROR IN ERASING EEPROM:%d,%d\n", i, ret);
						return -1;
					}
					page_number += 1;
				}
				kfree(erase_buf);
				break;
		default: printk("INVALID I2C FLASH COMMAND\n");
			 return -EINVAL;
	}
	return 0;
}

ssize_t i2c_flash_write(struct file *file, const char *buf,
		size_t count, loff_t *ppos){
	int copied, ret, page_number, size, i, j, k, i_var, page_calc;
	char *usr_buf, *tmp_buf;
	struct i2c_msg i2c_msg;

	page_number = i2c_flash_devp->page_nr;
	size = count * 64;
	usr_buf = kmalloc(size, GFP_KERNEL);
	copied = copy_from_user(usr_buf, buf, size);
	if(copied == 0){
		printk("SUCCESSFULLY COPIED\n");
	}
	else{
		printk("USR MSG COPY ERROR\n");
		return -ENOMEM;
	}

	for(i = 0; i < count; i++){
		tmp_buf = kmalloc((EEPROM_PAGE_SIZE + 2), GFP_KERNEL);
		printk("WRITING TO PAGE NUMBER:%d\n", page_number);
		i_var = i * 64;
		printk("I VAR:%d\n", i_var);
		page_calc = page_number;
		page_calc *= 64;
		tmp_buf[(i_var + 1) % 64] = (unsigned char)(page_calc & 0xff);
		page_calc &= 0xff00;
		tmp_buf[(i_var) % 64] = (unsigned char)((page_calc >> 8) & 0x7f);
		for(k = 2, j = i_var; j < i_var + 64; j++, k++){
			tmp_buf[k] = usr_buf[j];
		}
		i2c_msg.addr  = EEPROM_I2C_ADDR;
		i2c_msg.flags = 0;
		i2c_msg.len   = EEPROM_PAGE_SIZE;
		i2c_msg.buf   = tmp_buf;

		gpio_set_value_cansleep(I2C_LED, 1);
		ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, &i2c_msg, 1);
		gpio_set_value_cansleep(I2C_LED, 0);
		msleep(1);
		if(ret < 0){
			printk("I2C TRANSFER ERROR:%d\n", ret);
			return -1;
		}
		page_number = (page_number + 1) % 512;
		kfree(tmp_buf);
	}
	i2c_flash_devp->page_nr = page_number;
	kfree(usr_buf);
	return 0;
}

ssize_t i2c_flash_read(struct file *file, char *buf,
		size_t count, loff_t *ppos){
	int ret, bytes, page_number, page_calc, i, j, k, i_var;
	char *usr_buf, *read_msg, tmp_buf[2];	
	struct i2c_msg i2c_msg[2];

	usr_buf = kmalloc(EEPROM_PAGE_SIZE * count, GFP_KERNEL);
	page_number = i2c_flash_devp->page_nr;
	for(i = 0; i < count; i++){
		read_msg = kmalloc(EEPROM_PAGE_SIZE, GFP_KERNEL);
		memset(read_msg, 0x00, sizeof(read_msg));
		i_var = i * EEPROM_PAGE_SIZE;
		page_calc = page_number;
		page_calc *= 64;
		tmp_buf[1] = (unsigned char)(page_calc & 0xff);
		page_calc &= 0xff00;
		tmp_buf[0] = (unsigned char)((page_calc >> 8) & 0x7f);

		i2c_msg[0].addr	    = EEPROM_I2C_ADDR;
		i2c_msg[0].flags    = 0;
		i2c_msg[0].len      = 2;
		i2c_msg[0].buf      = tmp_buf;

		i2c_msg[1].addr     = EEPROM_I2C_ADDR;
		i2c_msg[1].flags    = I2C_M_RD;
		i2c_msg[1].len      = EEPROM_PAGE_SIZE;
		i2c_msg[1].buf      = read_msg;

		gpio_set_value_cansleep(I2C_LED, 1);
		ret = i2c_transfer(i2c_flash_devp->i2c_flash_adapter, i2c_msg, 2);
		gpio_set_value_cansleep(I2C_LED, 0);

		printk("READ_MSG:%s\n", read_msg);
		if(ret < 0){
			printk("I2C TRANSFER ERROR\n");
			return -1;
		}
		for(j = i_var, k = 0; j < i_var + EEPROM_PAGE_SIZE && k < EEPROM_PAGE_SIZE; j++, k++){
			usr_buf[j] = read_msg[k];
		}
		page_number = (page_number + 1) % 512;
		kfree(read_msg);
	}
	bytes = copy_to_user(buf, usr_buf, EEPROM_PAGE_SIZE * count); 
	if(bytes == 0){
		printk("SUCCESS\n");
	}
	else{
		printk("COPY TO USER ERROR\n");
	}
	i2c_flash_devp->page_nr = page_number;
	kfree(usr_buf);
	return 0;
}
		
static int i2c_flash_open(struct inode *inode, struct file *file){
	return 0;
}

static int i2c_flash_release(struct inode *inode, struct file *file){
	return 0;
}

int __init i2c_flash_init(void){
	int ret;
	int error, device_num;
	struct i2c_adapter *i2c_flash_adapter;
	struct i2c_client *i2c_flash_client;

	if(alloc_chrdev_region(&i2c_flash_major_number, 0, 1, DEVICE_NAME) < 0){
		printk(KERN_DEBUG "Can't register device\n");
		return -1;
	}

	i2c_flash_class = class_create(THIS_MODULE, DEVICE_NAME);
	device_num = MKDEV(MAJOR(i2c_flash_major_number), i2c_flash_minor_number);
	i2c_flash_devp = kmalloc(sizeof(struct i2c_flash_dev), GFP_KERNEL);
	if(!i2c_flash_devp){
		printk("I2C FLASH DEVP ERROR\n");
		return -ENOMEM;
	}

	cdev_init(&i2c_flash_devp->cdev, &i2c_flash_fops);
	i2c_flash_devp->cdev.owner	= THIS_MODULE;
	i2c_flash_devp->devno	 	= device_num;
	error = cdev_add(&i2c_flash_devp->cdev, device_num, 1);
	if(error){
		printk("CDEV ADD ERROR\n");
		return -1;
	}

	i2c_flash_devp->dev = device_create(i2c_flash_class, NULL, device_num, NULL, "i2c_flash");

	ret = gpio_request_one(I2C_MUX, GPIOF_OUT_INIT_LOW, "i2c_flash_pin");
	if(ret != 0){
		printk("GPIO EXPORT ERROR\n");
		return -1;
	}	

	gpio_set_value_cansleep(I2C_MUX, 0);

	ret = gpio_request_one(I2C_LED, GPIOF_OUT_INIT_LOW, "i2c_led");
	if(ret != 0){
		printk("GPIO IO 8 ERROR\n");
		return -1;
	}
	gpio_set_value_cansleep(I2C_LED, 0);

	i2c_flash_adapter = i2c_get_adapter(0);
	if(!i2c_flash_adapter){
		printk("I2C GET ADAPTER ERROR\n");
		return -1;
	}
	i2c_flash_devp->i2c_flash_adapter = i2c_flash_adapter;

	i2c_flash_client = i2c_new_device(i2c_flash_adapter, &i2c_flash_info);
	if(!i2c_flash_client){
		printk("I2C FLASH CLIENT NEW DEVICE ERROR\n");
		i2c_put_adapter(i2c_flash_adapter);
		return -1;
	}

	i2c_flash_devp->i2c_flash_client = i2c_flash_client;	
	i2c_flash_devp->page_nr = 0;
	printk("I2C FLASH DRIVER INITIALIZED\n");	
	return 0;
}

void __exit i2c_flash_exit(void){
	gpio_free(I2C_MUX);
	gpio_free(I2C_LED);
	cdev_del(&i2c_flash_devp->cdev);

	i2c_unregister_device(i2c_flash_devp->i2c_flash_client);
	i2c_put_adapter(i2c_flash_devp->i2c_flash_adapter);
	device_destroy(i2c_flash_class, i2c_flash_devp->devno);
	unregister_chrdev_region((i2c_flash_major_number), 1);

	kfree(i2c_flash_devp);
	
	class_destroy(i2c_flash_class);
	printk("\nI2C FLASH DRIVER REMOVED");
}

module_init(i2c_flash_init);
module_exit(i2c_flash_exit);
MODULE_LICENSE("GPL v2");

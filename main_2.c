#include <stdio.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <linux/i2c-dev.h>
#include <string.h>

int main(int argc, char **argv){
	int file;
	int ret;
	int adapter = 0;
	char high = 0x02;
	char low = 0x00;
	char send_message[16];
	memset((char*)&send_message, 0x00, sizeof(send_message));
	sprintf(send_message, "%d%dHello World\0",high, low);
	file = open("/dev/i2c-0", O_RDWR);
	if(file < 0){
		printf("\nERROR IN OPENING I2C FLASH\n");
		return 0;
	}
	printf("\nSUCCESSFULLY OPENED I2C FLASH\n");
	ret = write(file, &send_message, sizeof(send_message));
	if(ret != 0){
		printf("\nWRITE ERROR:%d", ret);
	}
	close(file);
	return 0;
}

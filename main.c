#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include <linux/ioctl.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#define I2C_FLASH_MAGIC 'k'
#define FLASHGETS           _IOR(I2C_FLASH_MAGIC, 1, int)
#define FLASHGETP           _IOR(I2C_FLASH_MAGIC, 2, char*)
#define FLASHSETP           _IOW(I2C_FLASH_MAGIC, 3, int)
#define FLASHERASE	    _IO(I2C_FLASH_MAGIC, 4)

int main(int argc, char **argv)
{
	int fd, res;
	char buff[128];
	int ret;
	int i = 0;
	int wp;
	int pg_nr = 0;
	unsigned long get_pg_nr;

	/* open devices */
	fd = open("/dev/i2c_flash", O_RDWR);
	if (fd < 0 ){
		printf("Can not open device file.\n");		
		return 0;
	}else{
		if(strcmp("show", argv[1]) == 0){
			memset(buff, 0x00, sizeof(buff));
			res = read(fd, buff, 2);
			printf("\nEEPROM DATA:%s", buff);
		}else if(strcmp("ioctl", argv[1]) == 0){
			if(strcmp("setp", argv[2]) == 0){
				printf("\nENTER THE PAGE NUMBER:");
				scanf("%d", &pg_nr);
				ret = ioctl(fd, FLASHSETP, pg_nr);
				if(ret == 0){
					printf("\nSUCCESS in setting page pointer");
				}
			}
			else if(strcmp("getp", argv[2]) == 0){
				ret = ioctl(fd, FLASHGETP, &get_pg_nr);
				if(ret == 0){
					printf("\nPAGE NUMBER:%ld", get_pg_nr); 
				}
			}
		}else if(strcmp("erase", argv[1]) == 0){
			printf("\nERASING EEPROM. THIS MAY TAKE A FEW SECONDS...");
			ret = ioctl(fd, FLASHERASE);
			if(ret == 0){
				printf("\nSUCCESS IN ERASING EEPROM");
			}
		}else if(strcmp("write", argv[1]) == 0){
			memset(buff, '2', sizeof(buff));
			memset(buff, '3', sizeof(buff)/2);
			res = write(fd, buff, 2);
			if(res == strlen(buff)+1){
				printf("Can not write to the device file.\n");		
				return 0;
			}
		}
		/* close devices */
		close(fd);
	}
	return 0;
}
